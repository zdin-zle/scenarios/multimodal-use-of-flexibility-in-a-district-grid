'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
This is a scenario for the Case Study **Flexibility´** using mosaik

This file attempts to integrate the flexibility simulators 

The first iteration calculates the operation plan withouth any considerations
of flexibility. This is done to get a first data set with the operation plan
with the flexibility parameters.

The second iteration includes the operational plans and integrates them to the
flexibility agents as Start Values, which are then used to optimize the 
flexibility schedules

The optimization is done using eocohda

'''
if __name__ == "__main__":
    import sys
    import os
    path = os.path.realpath(os.path.join(
        os.path.dirname(__file__), os.pardir, os.pardir))
    sys.path.append(path)

import copy
import logging
import mosaik
import random
from os.path import join
from mosaik.util import connect_many_to_one
from src.scenarios.util import assert_configurations, get_model_attrs, read_system_data, scale_system_data
from src.postprocessing.flex_comparison import plot_flex_comparison
from src.postprocessing.create_flex_pickles import create_flex_pickles
from src.common import *
from src.scenarios.scenario_config import SIM_CONFIG

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")


def create_world(configuration):
    """
    Creates a Mosaik world object based on the provided configuration.

    :param dict configuration: Dictionary containing configuration data for the scenario.

    :return: Mosaik world object or None if the scenario is not recognized.
    :rtype: mosaik.world object or None
    """
    random.seed(123456)
    if configuration["scenario"] == Scenarios.OP_PLAN:
        mosaik_config = {'addr': ('127.0.0.1', 5555)}
    elif configuration["scenario"] == Scenarios.FLEX:
        mosaik_config = {'addr': ('127.0.0.1', 8000)}
    else:
        return None
    world = mosaik.World(SIM_CONFIG, mosaik_config=mosaik_config)

    logging.info("Mosaik World Object Created")
    return world


def configure_scenario(world, configuration):
    """
    Configures a simulation scenario based on the provided world and configuration data.

    :param world: Mosaik world object.
    :param configuration: Dictionary containing configuration data for the scenario.
    :type world: mosaik.world object
    :type configuration: dict

    :return: Mosaik world object configured for the scenario.
    :rtype: mosaik.world 
    """
    scenario = configuration["scenario"]

    # Read all the required simulation data
    database_filename = join(
        RESULTS_DB_DIR, f"Results_{scenario.name}_{TIME_STAMP}.hdf5")
    meteo_data_filename = os.path.join(DATA_PROCESSED_DIR,
                                       "Braunschweig_meteodata_2020_bulding.csv")
    system_data = read_system_data()
    system_data_scaled = scale_system_data(system_data=system_data,
                                           scaling=configuration["scaling"])
    flex = False
    if scenario == Scenarios.FLEX:
        flex = True
    # simulation configuration parameters
    START = configuration["start"]
    END = configuration["end"]
    STEP_SIZE = configuration["step_size"]
    N_BUILDINGS = configuration["n_buildings"]

    if flex:
        temp_config = copy.deepcopy(configuration)
        temp_config["scenario"] = Scenarios.OP_PLAN
        database_op_plan_filename = join(RESULTS_DB_DIR,
                                         f"Results_{Scenarios.OP_PLAN.name}_{TIME_STAMP}.hdf5")
        flex_files = create_flex_pickles(configuration=temp_config,
                                         database_filename=database_op_plan_filename)
        params = {'control_id': 0, 'time_resolution': STEP_SIZE}  # Control ID

    ###############################
    #  Start the simulators
    ###############################
    db_sim = world.start('DB', step_size=STEP_SIZE, duration=END)
    meteo_sim = world.start('CSV',
                            sim_start=START,
                            datafile=meteo_data_filename)

    # One simulator for each building component to avoid mixing
    heatpump_sim_ = []
    tank_sim_ = []
    radiator_sim_ = []
    building_rc_sim_ = []
    heatpump_ctrl_sim_ = []
    tank_ctrl_sim_ = []

    keys_ = list(system_data_scaled['tank_data'].keys())
    for build in range(N_BUILDINGS):
        key = keys_[build]
        heatpump_sim_.append(world.start('HeatPumpSim', step_size=STEP_SIZE))
        tank_sim_.append(world.start('HotWaterTankSim',
                                     step_size=STEP_SIZE,
                                     config=system_data_scaled['tank_data'][key]))
        radiator_sim_.append(world.start('RadiatorSim', step_size=STEP_SIZE))
        building_rc_sim_.append(world.start("RCSim"))
        heatpump_ctrl_sim_.append(world.start("HeatpumpControllerSim",
                                  step_size=STEP_SIZE,
                                  start_date=START))
        tank_ctrl_sim_.append(world.start('TankControllerSim',
                                          step_size=STEP_SIZE,
                                          config={"min_temp_heatpump_entry": 20,
                                                  "min_T_upper": 45}))

    # Flexibility Components
    if scenario == Scenarios.FLEX:
        op_plan_sim_ = []
        pickle_sim = world.start('PickleSim',
                                 step_size=STEP_SIZE,
                                 pickle_files=flex_files,
                                 # duration = DURATION_H
                                 )

    # each simulator starts with a base operational plan:
        for idx in range(N_BUILDINGS):
            sim_params = {"pickle_file": flex_files[idx],
                          "participants": [x for x in range(N_BUILDINGS)], }
            op_plan_sim = world.start(
                'OpPlanSim', step_size=STEP_SIZE, **sim_params)
            op_plan_sim_.append(op_plan_sim)
        mas = world.start('MAS')

    ################################
    # Start Models
    ################################
    database_model = db_sim.Database(filename=database_filename)
    meteo_model = meteo_sim.Braunschweig_MeteoData.create(1)[0]

    # 1 Model for each simulator
    heatpump_model_ = []
    tank_model_ = []
    radiator_model_ = []
    building_rc_model_ = []
    heatpump_ctrl_model_ = []
    tank_ctrl_model_ = []
    flex_model_ = []
    agent_model_ = []
    op_plan_ = []

    for idx in range(N_BUILDINGS):
        # Heat Pumps
        heatpump_model = heatpump_sim_[
            idx].HeatPump.create(1,
                                 params=system_data_scaled['heat_pump_data'][str(idx)])
        heatpump_model_.append(heatpump_model[0])

        # Tanks
        tank_model = tank_sim_[idx].HotWaterTank(
            params=system_data_scaled['tank_data'][str(idx)],
            init_vals=system_data_scaled['tank_init_vals'][str(idx)])
        tank_model_.append(tank_model)

        # Radiators
        radiator_model = radiator_sim_[idx].Radiator.create(
            1,
            model_params=system_data_scaled['radiators_data'][str(idx)])
        radiator_model_.append(radiator_model[0])

        # Building RC Model
        building_rc_model = building_rc_sim_[idx].RCbuildingModel.create(
            1,
            init_vals=[system_data_scaled['building_data'][str(idx)]])
        building_rc_model_.append(building_rc_model[0])

        # HeatPump Controller
        heatpump_ctrl_model = heatpump_ctrl_sim_[idx].HeatpumpController.create(
            1,
            model_params=system_data_scaled['heat_pump_ctrl_data'][str(idx)])
        heatpump_ctrl_model_.append(heatpump_ctrl_model[0])

        # Tank Controller
        tank_ctrl_model = tank_ctrl_sim_[idx].TankController.create(
            1,
            controlled_entities=tank_model
        )
        tank_ctrl_model_.append(tank_ctrl_model[0])

        if flex:
            # Flexibility Data
            flex_model = pickle_sim.PickleSim()

            # Agent Model
            agent_model = mas.FlexAgent(**params)
            flex_model_.append(flex_model)

            # Operational Plan as Starting Values
            op_plan = op_plan_sim_[idx].OpPlanSim()
            op_plan_.append(op_plan)

            # Remember the FlexAgents entity for connecting it to the DB later:
            agent_model_.append(agent_model)

    ################################
    # Connect the simulators
    ################################
    for idx in range(N_BUILDINGS):
        # Meteo dat with building models
        world.connect(meteo_model, building_rc_model_[idx],
                      ('AirTemperature', 't_out'),
                      ('DirectNormalRad', 'normal_direct_radiation'),
                      ('DiffRadiation', 'horizontal_diffuse_radiation'),
                      ('DirNormIllum', 'normal_direct_illuminance'),
                      ('DirHorIllum', 'horizontal_diffuse_illuminance'),
                      ('Occupancy', 'occupancy'))

        # Buildings with heating device
        world.connect(building_rc_model_[idx], radiator_model_[idx],
                      ('heating_demand', 'Q_demand'),
                      ("t_air", "t_indoor"))

        # Connect the tank to the controller and back back to set the power
        world.connect(tank_model_[idx], tank_ctrl_model_[idx],
                      ("sensor_04.T", "T_upper_sensor"),
                      ("sensor_02.T", "T_heatpump_sensor"),
                      ("T_mean", "T_mean_tank"),
                      ("mass", "mass"))

        world.connect(meteo_model, tank_ctrl_model_[idx],
                      ('AirTemperature', 'T_air'))

        world.connect(tank_ctrl_model_[idx], tank_model_[idx],
                      ("P_th_set", "hr_1.P_th_set"),
                      time_shifted=True,
                      initial_data={"P_th_set": 0})

        # Air source for the heat pump
        world.connect(meteo_model, heatpump_model_[idx],
                      ('AirTemperature', 'heat_source_T'),
                      ('AirTemperature', 'T_amb'))

        # Connect the Radiator with the Hot Water Tank and back for return
        world.connect(tank_model_[idx], radiator_model_[idx],
                      ('cc_out.T', 't_supply'))

        world.connect(radiator_model_[idx], tank_model_[idx],
                      ('t_supply', 'cc_out.T'),
                      ('t_return', 'cc_in.T'),
                      ('massflow_in', 'cc_in.F'),
                      ('massflow_out', 'cc_out.F'),
                      time_shifted=True,
                      initial_data={'t_supply': 50})

        # Connect the Heat Pump with the Hot Water Tank and back for return
        world.connect(tank_model_[idx], heatpump_model_[idx],
                      ('hp_in.T', 'cond_in_T'))

        world.connect(heatpump_model_[idx], tank_model_[idx],
                      ('cond_in_T', 'hp_in.T'),
                      ('cond_m', 'hp_in.F'),
                      ('cond_m_neg', 'hp_out.F'),
                      time_shifted=True,
                      initial_data={"cond_in_T": 50})

        # Heatpump controller
        world.connect(building_rc_model_[idx], heatpump_ctrl_model_[idx],
                      ('heating_demand', 'Q_demand_signal'))

        world.connect(tank_model_[idx], heatpump_ctrl_model_[idx],
                      ('sensor_01.T', "T_in_tank"),
                      ("T_mean", "T_mean_tank"))

        world.connect(heatpump_ctrl_model_[idx], heatpump_model_[idx],
                      ('Q_Demand_controlled', 'Q_Demand'))

        if scenario == Scenarios.FLEX:
            world.connect(flex_model_[idx], agent_model_[idx], ('output', 'Flexibility'),
                          async_requests=True)
            world.connect(op_plan_[idx], agent_model_[idx], ('StartValues', 'StartValues'),
                          async_requests=True)
            world.connect(agent_model_[idx], tank_ctrl_model_[idx], ('FlexSchedules', "signal"),
                          async_requests=True,)

    # Connect all the relevant data to the database for saving
    for idx in range(N_BUILDINGS):
        world.connect(meteo_model, database_model, "AirTemperature")

        if flex:
            connect_many_to_one(world, agent_model_,
                                database_model, 'FlexSchedules')

        for attr in get_model_attrs(heatpump_model_[0]):
            world.connect(heatpump_model_[idx], database_model, attr)

        for attr in get_model_attrs(radiator_model_[0]):
            world.connect(radiator_model_[idx], database_model, attr)

        for attr in get_model_attrs(heatpump_ctrl_model_[0]):
            world.connect(heatpump_ctrl_model_[idx],
                          database_model, attr)

        for attr in get_model_attrs(building_rc_model_[0]):
            world.connect(building_rc_model_[idx], database_model, attr)

        for attr in get_model_attrs(tank_ctrl_model_[0]):
            world.connect(tank_ctrl_model_[idx], database_model, attr)

        for attr in get_model_attrs(tank_model_[0]):
            if attr not in ['_', 'sh_supply', 'sh_demand', 'dhw_demand',
                            'dhw_supply', 'hp_demand', 'hp_supply',
                            'step_executed', "snapshot", "snapshot_connections"]:
                world.connect(tank_model_[idx], database_model, attr)

    return world


def run_scenario(configuration):
    '''
    Chain of methods to fully run a scenario

    Example::

        configuration = {
            "scenario": Scenarios.FLEX,
            "scaling": 5,
            "start": '2020-01-01 00:00:00',
            "end": 7*24*3600,
            "step_size": 3600,
            "n_buildings": 6
        }


    :param configuration: Dictionary with configuration parameters
    :type configuration: dict



    '''
    scenario = configuration["scenario"]
    logging.info(f"Working on scenario '{scenario.name}'")

    world = create_world(configuration)
    world = configure_scenario(world, configuration)
    assert_configurations(world, configuration)

    END = configuration["end"]
    world.run(until=END, print_progress=True)
    database_filename = join(
        RESULTS_DB_DIR, f"Results_{scenario.name}_{TIME_STAMP}.hdf5")

    logging.info(f"Results saved to {database_filename}")

    return None


def main(configuration=None):
    """
    Runs a complete simulation for both scenarios (OP_PLAN and FLEX) to create the
    operational plans and calculate flexibilities. Uses a `configuration` dictionary, such as::

        configuration = {
            "scenario": Scenarios.FLEX,
            "scaling": 5,
            "start": '2020-01-01 00:00:00',
            "end": 7*24*3600,
            "step_size": 3600,
            "n_buildings": 6
        }


    :param configuration: Dictionary with configuration parameters
    :type configuration: dict

    :return: None
    """

    if configuration is None:
        configuration = {
            "scenario": Scenarios.OP_PLAN,
            "scaling": 5,
            "start": '2020-01-01 00:00:00',
            "end": 7*24*3600,
            "step_size": 3600,
            "n_buildings": 6
        }

    configuration["scenario"] = Scenarios.OP_PLAN
    run_scenario(configuration)
    configuration["scenario"] = Scenarios.FLEX
    run_scenario(configuration)

    database_op_plan = join(
        RESULTS_DB_DIR, f"Results_{Scenarios.OP_PLAN.name}_{TIME_STAMP}.hdf5")
    database_flex = join(
        RESULTS_DB_DIR, f"Results_{Scenarios.FLEX.name}_{TIME_STAMP}.hdf5")
    plot_flex_comparison(configuration, database_op_plan,
                         database_flex, model="HotWaterTank")
    plot_flex_comparison(configuration, database_op_plan,
                         database_flex, model="Radiator")


if __name__ == "__main__":
    configuration = {
        "scenario": Scenarios.FLEX,
        "scaling": 5,
        "start": '2020-01-01 00:00:00',
        "end": 7*24*3600,
        "step_size": 3600,
        "n_buildings": 6
    }

    main(configuration=configuration)
