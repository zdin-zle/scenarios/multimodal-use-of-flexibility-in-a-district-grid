'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
Configuration dictionary with the declaration of the
simulators to be used by the mosaik simulator.
'''

SIM_CONFIG = {
    'HotWaterTankSim': {
        'python': 'mosaik_heatpump.hotwatertanksim.hotwatertank_mosaik:HotWaterTankSimulator',
    },
    'HeatPumpSim': {
        'python': 'mosaik_heatpump.heatpump.Heat_Pump_mosaik:HeatPumpSimulator',
    },
    'RadiatorSim': {
        'python': 'src.components.radiator.radiator_simulator:RadiatorSimulator',
    },
    'HeatpumpControllerSim': {
        'python': 'src.components.thermal_controller.heatpump_controller_simulator:HeatpumpControllerSimulator',
    },
    'TankControllerSim': {
        'python': 'src.components.thermal_controller.tank_controller_simulator:TankControllerSimulator',
    },
    'CSV': {
        'python': 'mosaik_csv:CSV'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5',
    },
    'RCSim': {
        'python': 'mosaik_building.building_simulator:RCbuildingSim'
    },
    'MAS': {
        'python': 'mosaik_cohda.cohda_simulator:CohdaSimulator'
    },
    'PickleSim': {
        'python': 'mosaik_cohda.pickles.pickle_sim:PickleSim'
    },
    'OpPlanSim': {
        'python': 'mosaik_cohda.pickles.op_plan_sim:OpPlanSim'
    },
}

