"""
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
This package contains the main files for building and running a simulation of the scenario.
"""