'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
Common parameters to be used through the src and subfolders.
Contains PATHS, SCENARIOS helper functions.
'''
from datetime import datetime
from enum import Enum
import os
from os.path import join

VERBOSE = False

def get_project_root():
    """Returns the path to the project root directory.

    :return: A string with the project root directory.
    :rtype: str
    """ 
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir,
    ))

# Paths
BASE_DIR = get_project_root() 
BASE_DIR_SRC = join(BASE_DIR, "src")
DATA_DIR = join(BASE_DIR_SRC, "district_data")
DATA_PROCESSED_DIR = join(DATA_DIR, "processed")
DATA_RAW_DIR = join(DATA_DIR, "raw")
RESULTS_DIR = join(BASE_DIR_SRC, "results")
RESULTS_DB_DIR = join(RESULTS_DIR, "databases")
RESULTS_FIGS_DIR = join(RESULTS_DIR, "figures")
RESULTS_KIPS_DIR = join(RESULTS_DIR, "kpis")
RESULTS_OP_PLAN_DIR = join(RESULTS_DIR, "op_plan")
SCENARIOS_DIR = join(BASE_DIR_SRC, "scenarios")
PREPROC_DIR = join(BASE_DIR_SRC, "preprocessing")
POSTPROC_DIR = join(BASE_DIR_SRC, "postprocessing")
TEST_DIR = join(BASE_DIR, "tests")
TIME_STAMP =datetime.now().strftime("%Y%m%d%H%M%S")



DIRECTORIES = [
        BASE_DIR_SRC, DATA_DIR, DATA_PROCESSED_DIR, DATA_RAW_DIR, 
        RESULTS_DIR, RESULTS_DB_DIR, RESULTS_FIGS_DIR, 
        RESULTS_KIPS_DIR, RESULTS_OP_PLAN_DIR, SCENARIOS_DIR, PREPROC_DIR,
        POSTPROC_DIR, TEST_DIR
    ]

def create_directories(directories):
    """
    Checks if specified directories exist. If they do not exist, creates the directories.

    :raises OSError: If there's an issue creating directories.
    """

    for directory in directories:
        try:
            if not os.path.exists(directory):
                os.makedirs(directory)
                print(f"Created directory: {directory}")
            else:
                pass
                # print(f"Directory already exists: {directory}")
        except OSError as e:
            raise OSError(f"Failed to create directory '{directory}': {e}")


class Scenarios(Enum):
    '''
    List of the available Simulation Scenarios
    
    :TEST: Reserved for testing and development
    :OP_PLAN: The scenario has no agents or flexibility calculaitons
    :FLEX: Include flexibility calculations for optimization of load distribution
    '''

    TEST = 0 # Reserved for testing and development
    OP_PLAN = 1 # removes flexibilites to calculate the operational plan
    FLEX = 2 # flexibility scenario

