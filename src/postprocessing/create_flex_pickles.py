"""
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**
Jan Philipp Hörding **@UOL/OFFIS**

Description 
-------------------------------
This script contains functions to process simulation results stored in an HDF5 database file related to a specific scenario (Scenarios.OP_PLAN). It facilitates the extraction and organization of flexibility data for each building from the simulation results and subsequently saves this information as pickle files.

Functionality
-------------------------------
- create_flex_dict:
    Extracts flexibility data for each building from simulation results stored in an HDF5 database file, organized based on configuration parameters. It returns a list of dictionaries containing flexibility information.
    
- save_dicts_to_pickle:
    Saves a list of dictionaries as separate pickle files in a specified folder (or default folder if not provided), returning a dictionary mapping integer indexes to corresponding file names.

- create_flex_pickles: 
    Orchestrates the process of creating flexibility dictionaries from simulation data and saving them as pickle files based on provided configuration parameters.

Note
-------------------------------
The script assumes a specific scenario `(Scenarios.OP_PLAN)` and handles flexibility data extraction and storage accordingly.
"""


if __name__ == "__main__":
    import sys
    import os
    path = os.path.realpath(os.path.join(
        os.path.dirname(__file__), os.pardir, os.pardir))
    sys.path.append(path)

import logging
import h5py
import os
import pickle
from pprint import pprint
from os.path import join
from src.common import Scenarios, RESULTS_DB_DIR, RESULTS_OP_PLAN_DIR

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")


def create_flex_dict(configuration, database_filename):
    """
    Creates a dictionary containing flexibility data from simulation results.

    :param dict configuration: Configuration parameters for the simulation.
    :param str database_filename: Path to the HDF5 database file.

    :return: List of dictionaries containing flexibility data for each building.
    :rtype: list[dict]
    """
    # Define paths
    scenario = configuration["scenario"]

    if scenario not in [Scenarios.OP_PLAN]:
        raise ValueError(f"Scenario can be only {Scenarios.OP_PLAN.name} but {scenario.name} was given")    

    if database_filename is None:
        database_filename = join(RESULTS_DB_DIR, f"Results_{scenario.name}.hdf5")
    
    database = h5py.File(database_filename, "r")

    OUTPUT_DIR = join(RESULTS_OP_PLAN_DIR, scenario.name)

    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    # Get databases entries
    series = {}
    series["tank"] = [b for b in database["Series"].keys()
                      if "HotWaterTankSim" in b]
    series["tank_ctrl"] = [
        b for b in database["Series"].keys() if "TankControllerSim" in b]

    N_BUILDINGS = configuration["n_buildings"]

    flex_list = []

    # Get flexibility data for each building
    for n in range(N_BUILDINGS):
        flex = {}
        tank_key = series["tank"][n]
        data_tank = database["Series"][tank_key]
        p_max = [x for x in data_tank['hr_1.P_th_max']] #TODO check for temperatures
        p_min = [x for x in data_tank['hr_1.P_th_min']]
        mean_T = [x for x in data_tank['T_mean']]
        p_op_plan = [x for x in data_tank['hr_1.P_th']]

        mass = [x/1000 for x in data_tank['mass']] # mass is in grams??- Yes. Confirmed

        tank_ctrl_key = series["tank_ctrl"][n]
        data_tank_ctrl = database["Series"][tank_ctrl_key]
        max_T = data_tank_ctrl["T_max"]

        # Write flexibility entries
        C_P = 4180

        flex['schedule']= p_op_plan
        flex['max_energy_delta'] = [
            mass[i] * (max_T[i] - mean_T[i]) * C_P for i in range(len(mass))]
        flex['flex_max_power'] = p_max
        flex['flex_min_power'] = p_min
        flex['flex_max_energy_delta'] = [max(p_max[i]*3600,flex["max_energy_delta"][i]) for i in range(len(mass))]
        flex['flex_min_energy_delta'] = [0] * len(p_max)
        flex_list.append(flex)

    return flex_list


def save_dicts_to_pickle(flex_:list, configuration:dict, folder_name:str=None)-> dict: 
    """
    Save a list of dictionaries as separate pickle files in a folder called "FOLDER".

    :param list[dict] flex_: List of dictionaries to be saved.
    :param dict configuration: Configuration parameters for the simulation.
    :param str folder_name: Name of the folder to save pickle files. Defaults to None.

    :return: A dictionary containing integer indexes as keys and corresponding file names as values.
    :rtype: dict[int, str]
    """
    # Create a folder if it doesn't exist
    scenario = configuration["scenario"]
    if folder_name is None:
        folder_name = join(RESULTS_OP_PLAN_DIR, scenario.name)

    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    # Dictionary to store file names
    file_names = {}

    # Save each dictionary separately in pickle files
    for index, dictionary in enumerate(flex_):
        file_name = os.path.join(folder_name, f"flexibility_{index}.pickle")
        with open(file_name, 'wb') as file:
            pickle.dump(dictionary, file)
        file_names[index] = file_name

    # Return the dictionary containing file names
    logging.info(f"Pickles with flexibility data saved to {folder_name}")

    return file_names


def create_flex_pickles(configuration, database_filename):
    """
    Orchestrates the creation and saving of flexibility dictionaries as pickle files.

    :param dict configuration: Configuration parameters for the simulation.
    :param str database_filename: Path to the HDF5 database file.

    :return: A dictionary containing integer indexes as keys and corresponding file names as values.
    :rtype: dict[int, str]
    """
    flex_ = create_flex_dict(configuration, database_filename)
    saved_files = save_dicts_to_pickle(flex_, configuration)
    return saved_files


if __name__ == "__main__":
    configuration = {
        "scenario": Scenarios.OP_PLAN,
        "scaling": 5,
        "start": '2020-01-01 00:00:00',
        "end": 7*24*3600,
        "step_size": 3600,
        "n_buildings": 6
    }

    files = create_flex_pickles(configuration)
    pprint(files)
