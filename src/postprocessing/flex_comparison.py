"""
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
This script contains functions to load, process, and plot comparisons between data obtained from operational plan simulations and flexibility simulations. It enables the visualization of comparison results for specific models, facilitating insights into co-simulation scenarios.

Functionality
-------------------------------
- load_data(file_path):
    Loads data from an HDF5 file.
  
- get_series_keys:
    Retrieves series keys based on the provided model name.
  
- prepare_comparison_data:
    Prepares data required for comparison by extracting series keys, parameters, start and end times.
  
- create_comparison_df:
    Creates a Pandas DataFrame for comparison by collating and structuring data from both operational plan and flexibility simulations.
  
- plot_comparison:
    Plots comparisons between tank and flexibility columns, generating line plots for visualization purposes.
  
- plot_flex_comparison:
    Orchestrates the loading, processing, and plotting of comparison data between operational plan and flexibility simulations for a specified model.

Note
-------------------------------
The script utilizes several libraries such as Matplotlib, Pandas, Seaborn, and h5py for data visualization, manipulation, and file handling.
"""
import matplotlib
import warnings
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import logging
import h5py
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from src.common import *
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")

# Ignore MatplotlibDeprecationWarning
warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)



def load_data(file_path):
    """
    Loads data from an HDF5 file.

    :param str file_path: Path to the HDF5 file.
    :return: HDF5 file object.
    """
    return h5py.File(file_path, "r")


def get_series_keys(data, model):
    """
    Retrieves series keys based on the provided model name.

    :param data: HDF5 file object representing data loaded from an HDF5 file.
    :type data: h5py.File
    :param str model: Model name for filtering series keys.
    :return: List of series keys.
    :rtype: list
    """
    return [b for b in data["Series"].keys() if model.lower() in b.lower()]


def prepare_comparison_data(data_op_plan, data_flex, model_name, configuration):
    """
    Prepares data required for comparison.

    :param data_op_plan: Data loaded from the operational plan HDF5 file.
    :type data_op_plan: h5py.File
    :param data_flex: Data loaded from the flexibility HDF5 file.
    :type data_flex: h5py.File
    :param str model_name: Model name for filtering series keys.
    :param dict configuration: Configuration details for comparison.
    :return: Tuple containing series, params, start time, and end time.
    :rtype: tuple
    """
    series = {}
    series[model_name] = get_series_keys(data_op_plan, model_name)

    params = [p for p in data_op_plan["Series"][series[model_name][0]].keys()]
    START = configuration["start"]
    end_time = configuration["end"]

    return series, params, START, end_time


def create_comparison_df(data_op_plan, data_flex, model_name, params, START, end_time, series):
    """
    Creates a DataFrame for comparison.

    :param data_op_plan: Data loaded from the operational plan HDF5 file.
    :type data_op_plan: h5py.File
    :param data_flex: Data loaded from the flexibility HDF5 file.
    :type data_flex: h5py.File
    :param str model_name: Model name for filtering series keys.
    :param list params: List of parameters for comparison.
    :param str START: Start time for the comparison.
    :param int end_time: End time for the comparison.
    :param dict series: Dictionary containing series keys.
    :return: Pandas DataFrame for comparison.
    :rtype: pd.DataFrame
    """
    data_comparison = {}
    df_ = {}
    for param in params:
        for i in range(len(series[model_name])):
            data_comparison[f"{model_name}{i}"] = data_op_plan["Series"][series[model_name][i]][param]
            data_comparison[f"{model_name}{i}_Flex"] = data_flex["Series"][series[model_name][i]][param]

        periods = len(data_comparison[f"{model_name}{i}_Flex"])

        dti = pd.date_range(start=START, periods=periods, freq="1H")
        data_comparison["Time"] = dti
        df = pd.DataFrame.from_dict(data_comparison)
        df.set_index("Time", inplace=True)
        df_[param]=df
    return df_


def plot_comparison(df, model_name, param,series):
    """
    Plots the comparison between tank and flexibility columns.

    :param df: DataFrame containing comparison data.
    :type df: pd.DataFrame
    :param str model_name: Model name for filtering series keys.
    :param str param: Parameter for which comparison is performed.
    :param dict series: Dictionary containing series keys.
    :return: None
    """
    colors = plt.cm.get_cmap('tab10', len(df.columns) // 2)

    plt.figure(figsize=(10, 6))
    for i in range(0, len(series[model_name])):
        tank_col = f'{model_name}{i}'
        flex_col = f'{model_name}{i}_Flex'
        color = colors(i)
        plt.plot(df.index, df[tank_col], label=f'{tank_col}', linestyle='-', linewidth=2, color=color)
        plt.plot(df.index, df[flex_col], label=f'{flex_col}', linestyle='--', linewidth=2, color=color)

    FONT = "sans serif"
    plt.xlabel('Time', fontsize=12, fontname=FONT)
    plt.ylabel(param, fontsize=12, fontname=FONT)
    plt.title(f"Co-Simulation Results \n {model_name}-{param}", fontsize=14, fontname=FONT)
    plt.legend()
    plt.grid(True)
    plt.xticks(fontsize=10, fontname=FONT)
    plt.yticks(fontsize=10, fontname=FONT)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', fontsize=10)
    plt.tight_layout()
    figures_folder = os.path.join(RESULTS_FIGS_DIR, TIME_STAMP)
    create_directories([figures_folder])
    fig_path = os.path.join(figures_folder,
        f"Flex_Comparison_{model_name}_{param}.png")
    plt.savefig(fig_path, bbox_inches='tight', dpi=600)
    logging.info(f"Figure saved to: {fig_path}")
    plt.close()

def plot_flex_comparison(configuration, database_op_plan, database_flex, model="HotWaterTank"):
    """
    Orchestrates the loading, processing, and plotting of the comparison data.

    :param dict configuration: Configuration details for comparison.
    :param str database_op_plan: Path to the operational plan HDF5 file.
    :param str database_flex: Path to the flexibility HDF5 file.
    :param str model: Model name for analysis. Defaults to "HotWaterTank".
    :return: None
    """
    sns.set_style("whitegrid")

    data_op_plan = load_data(database_op_plan)
    data_flex = load_data(database_flex)

    model_name = model.lower()

    series, params, START, end_time = prepare_comparison_data(data_op_plan, data_flex, model_name, configuration)
    df_ = create_comparison_df(data_op_plan, data_flex, model_name, params, START, end_time, series)

    for param in params:
        plot_comparison(df_[param], model_name, param, series)

    return None

