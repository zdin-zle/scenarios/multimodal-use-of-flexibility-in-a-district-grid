"""
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------
This script contains functions to process data from an Excel file containing building, 
heat pump, radiator, and tank data. It organizes and saves the processed data as JSON files for further use in the simulation scenario.

List of Functions
-------------------------------
- create_building_json_data 
    Processes building data from an Excel file, organizes it, and saves the result as a JSON file.
    
- create_heat_pump_data
    Processes heat pump data from an Excel file, organizes it, and saves the result as a JSON file.
    
- create_radiators_data
    Processes radiator data from an Excel file, organizes it, and saves the result as a JSON file.
    
- create_tank_data
    Processes tank data from an Excel file, organizes it, and saves the result as a JSON file.
    
- create_tank_init_vals(n_tanks=6, raw_filename="Zone_Building_Data.xlsx"): 
    Processes initial tank data from an Excel file, organizes it, and saves the result as a JSON file.
    
- create_heat_controller_data
    Creates a JSON file with heat pump controller data.
    
- create_heat_system_data 
    Orchestrates the creation of system data by calling various functions to read and process data from an Excel file, then returns a dictionary containing paths to the saved JSON files for building, heat pump, radiator, tank, tank initialization values, and heat pump controller data.

"""
if __name__ == "__main__":
    import os
    import sys

    path = os.path.realpath(os.path.join(
        os.path.dirname(__file__), os.pardir, os.pardir))
    sys.path.append(path)
import copy
import numpy as np
import logging
from src.common import DATA_PROCESSED_DIR, DATA_RAW_DIR
from os.path import join
import pandas as pd
import json

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S")


def create_building_json_data(raw_filename="Zone_Building_Data.xlsx"):
    """
    Reads building data from an Excel file, processes it, and saves the result as a JSON file.

    :param str raw_filename: The name of the raw data file. Defaults to "Zone_Building_Data.xlsx".

    :return: Path to the saved JSON file.
    :rtype: str
    """

    raw_data_path = join(DATA_RAW_DIR, raw_filename)

    xls = pd.ExcelFile(raw_data_path)
    sheet_names = [name for name in xls.sheet_names if "Building" in name]
    buildings = {}

    for sheet in sheet_names:
        building_data = pd.read_excel(
            raw_data_path, sheet_name=sheet, skiprows=5)
        building_data = building_data[["Name", "Value"]]
        building_dict = {}

        for idx, row in building_data.iterrows():
            name = row["Name"]
            val = row["Value"]
            building_dict[name] = val

        buildings[sheet] = building_dict

    building_models = pd.read_excel(raw_data_path, sheet_name="Models")
    building_models_list = {}

    for _, rows in building_models.iterrows():
        idx = rows["ID"]
        name = rows["Building Data"]
        building_models_list[idx] = buildings[name]

    buildings_models_dir = join(DATA_PROCESSED_DIR, "building_models.json")

    with open(buildings_models_dir, "w") as f:
        json.dump(building_models_list, f, indent=4)

    logging.info(f"Building data saved to {buildings_models_dir}")

    return buildings_models_dir


def create_heat_pump_data(raw_filename="Zone_Building_Data.xlsx"):
    """
    Reads heat pump data from an Excel file, processes it, and saves the result as a JSON file.

    :param str raw_filename: The name of the raw data file. Defaults to "Zone_Building_Data.xlsx".

    :return: Path to the saved JSON file.
    :rtype: str
    """
    raw_data_path = join(DATA_RAW_DIR, raw_filename)
    hp_data = pd.read_excel(
        raw_data_path, sheet_name="HeatPumps", index_col="ID")
    hp_dict = {}
    params = hp_data.columns

    for idx, row in hp_data.iterrows():
        hp_model = {}
        for par in params:
            hp_model[par] = row[par]
        hp_dict[int(idx)] = hp_model

    hp_models_dir = join(DATA_PROCESSED_DIR, "hp_models.json")

    with open(hp_models_dir, "w") as f:
        json.dump(hp_dict, f, indent=4)

    logging.info(f"Heat Pumps data saved to {hp_models_dir}")

    return hp_models_dir


def create_radiators_data(raw_filename="Zone_Building_Data.xlsx"):
    """
    Reads radiator data from an Excel file, processes it, and saves the result as a JSON file.

    :param str raw_filename: The name of the raw data file. Defaults to "Zone_Building_Data.xlsx".

    :return: Path to the saved JSON file.
    :rtype: str
    """
    raw_data_path = join(DATA_RAW_DIR, raw_filename)
    radiator_data = pd.read_excel(
        raw_data_path, sheet_name="Radiators", index_col="ID")
    radiator_dict = {}
    params = radiator_data.columns

    for idx, row in radiator_data.iterrows():
        radiator_model = {}
        for par in params:
            radiator_model[par] = row[par]
        radiator_dict[int(idx)] = radiator_model

    radiators_models_dir = join(DATA_PROCESSED_DIR, "radiator_models.json")

    with open(radiators_models_dir, "w") as f:
        json.dump(radiator_dict, f, indent=4)

    logging.info(f"Heat Pumps data saved to {radiators_models_dir}")

    return radiators_models_dir


def create_tank_data(raw_filename="Zone_Building_Data.xlsx"):
    """
    Reads tank data from an Excel file, processes it, and saves the result as a JSON file.

    :param str raw_filename: The name of the raw data file. Defaults to "Zone_Building_Data.xlsx".

    :return: Path to the saved JSON file.
    :rtype: str
    """
    base_dict = {
        'height': 2100,
        'diameter': 1200,
        'T_env': 14.0,
        'htc_walls': 1.0,
        'htc_layers': 20,
        'n_layers': 5,
        'n_sensors': 5,
        'connections': {
            'cc_in': {'pos': 200},
            'cc_out': {'pos': 1900},
            'hp_in': {'pos': 1600},
            'hp_out': {'pos': 400},
        },
        'heating_rods': {
            'hr_1': {'pos': 1000,
                     'P_th_stages': [0, 500, 1000, 2000, 4000, 6000],
                     "T_max": 90,
                     "eta": 1,
                     'P_th_set': 0}
        }
    }

    raw_data_path = join(DATA_RAW_DIR, raw_filename)
    tank_data = pd.read_excel(
        raw_data_path, sheet_name="Tanks", index_col="ID")
    tank_dict = {}

    for idx, row in tank_data.iterrows():
        tank_model = copy.deepcopy(base_dict)
        tank_model['height'] = float(row['height'])
        tank_model['diameter'] = float(row['diameter'])
        tank_model['T_env'] = float(row['T_env'])
        tank_model['htc_walls'] = float(row['htc_walls'])
        tank_model['htc_layers'] = float(row['htc_layers'])
        tank_model['n_layers'] = float(row['n_layers'])
        tank_model['n_sensors'] = float(row['n_sensors'])

        # Connections data
        tank_model['connections']['cc_in']['pos'] = float(
            row['connections.cc_in.pos'])
        tank_model['connections']['cc_out']['pos'] = float(
            row['connections.cc_in.pos'])
        tank_model['connections']['hp_in']['pos'] = float(
            row['connections.hp_in.pos'])
        tank_model['connections']['hp_out']['pos'] = float(
            row['connections.hp_out.pos'])

        # Heating roads data
        tank_model['heating_rods']['hr_1']['pos'] = float(
            row['heating_rods.hr_1.pos'])

        a = float(row['heating_rods.hr_1.P_th_stages'])
        tank_model['heating_rods']['hr_1']['P_th_stages'] = [
            float(x) for x in np.linspace(0, a, int(a / 1000) + 1)]
        tank_model['heating_rods']['hr_1']['T_max'] = float(
            row['heating_rods.hr_1.T_max'])
        tank_model['heating_rods']['hr_1']['eta'] = float(
            row['heating_rods.hr_1.eta'])
        tank_model['heating_rods']['hr_1']['P_th_set'] = float(
            row['heating_rods.hr_1.P_th_set'])

        tank_dict[int(idx)] = tank_model

    tank_models_dir = join(DATA_PROCESSED_DIR, "tank_models.json")

    with open(tank_models_dir, "w") as f:
        json.dump(tank_dict, f, indent=4)

    logging.info(f"Tank data saved to {tank_models_dir}")

    return tank_models_dir


def create_tank_init_vals(n_tanks=6, raw_filename="Zone_Building_Data.xlsx"):
    """
    Reads tank initial data and saves the result as a JSON file.

    :param int n_tanks: Number of tanks. Defaults to 6.
    :param str raw_filename: The name of the raw data file. Defaults to "Zone_Building_Data.xlsx".

    :return: Path to the saved JSON file.
    :rtype: str
    """
    raw_data_path = join(DATA_RAW_DIR, raw_filename)
    tank_init_data = pd.read_excel(
        raw_data_path, sheet_name="Tank_init_vals", index_col="ID")

    tanks_vals = {}
    for idx in range(n_tanks):
        tank_init_vals = {'layers': {'T': [float(tank_init_data.loc[idx, f"T{x+1}"]) for x in range(5)]},
                            'hr_1': {'P_th_set': float(tank_init_data.at[idx, "P_th_set"])}
                            }
        tanks_vals[idx] = tank_init_vals

    tank_init_vals_dir = join(DATA_PROCESSED_DIR, "tank_init_vals.json")

    with open(tank_init_vals_dir, "w") as f:
        json.dump(tanks_vals, f, indent=4)

    logging.info(f"Tank initial values saved to {tank_init_vals_dir}")

    return tank_init_vals_dir


def create_heat_controller_data(n_hp=6):
    """
    Creates a JSON file with the heat pump controller data.

    :param int n_hp: Number of heat pumps. Defaults to 6.

    :return: Path to the saved JSON file.
    :rtype: str
    """
    controller_vals = {}
    for idx in range(n_hp):
        heatpump_controller_params = {"min_temperature": 25,
                                      "max_input_temperature": 70,
                                      "min_tank_mean_T": 30,
                                      "Q_demand_min": 0,
                                      }
        controller_vals[idx] = heatpump_controller_params

    hp_ctrl_vals_dir = join(DATA_PROCESSED_DIR, "hp_ctr_vals.json")

    with open(hp_ctrl_vals_dir, "w") as f:
        json.dump(controller_vals, f, indent=4)

    logging.info(f"Heat Pump controller values saved to {hp_ctrl_vals_dir}")

    return hp_ctrl_vals_dir


def create_heat_system_data():
    """
    Orchestrates the creation of system data by calling various functions to read and process data, 
    then returns a dictionary containing paths to the saved JSON files.

    :return: Dictionary containing paths to the saved JSON files for building, heat pump, radiator, tank, tank initialization values, and heat pump controller data.
    :rtype: dict
    """
    building_data = create_building_json_data()
    heat_pump_data = create_heat_pump_data()
    radiators_data = create_radiators_data()
    tank_data = create_tank_data()
    tank_init_vals = create_tank_init_vals()
    heat_pump_ctrl_data = create_heat_controller_data()

    heat_system_data = {}
    heat_system_data['building_data'] = building_data
    heat_system_data['heat_pump_data'] = heat_pump_data
    heat_system_data['radiators_data'] = radiators_data
    heat_system_data['tank_data'] = tank_data
    heat_system_data['tank_init_vals'] = tank_init_vals
    heat_system_data['heat_pump_ctrl_data'] = heat_pump_ctrl_data
    return heat_system_data


if __name__ == "__main__":
    data_dict = create_heat_system_data()
    print(data_dict)
    
