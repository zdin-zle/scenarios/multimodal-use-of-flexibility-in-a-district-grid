'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Description
-------------------------------

The RadiatorSimulator class is a component designed to work within the mosaik cosimulation framework. 
It models a simulation of radiator systems by creating, initializing, and simulating entities based 
on specified parameters

'''
import mosaik_api
from src.components.radiator.radiator import Radiator
import itertools
import numpy as np

# Simulation Metadata
META = {
    'type': 'time-based',
    'models': {
        'Radiator': {
            'public': True,
            'params': ["model_params"],
            'attrs': [
                "t_supply",         # Radiator supply temperature [C]
                "t_return",         # Radiator return temperature [C]
                "t_indoor",         # Indoor temperature [C]
                "Q_demand",         # Heat Demand [W]
                "massflow_in",         # Radiator massflow  in [kg/s]
                "massflow_out",         # Radiator massflow  in [kg/s]
                "signal",    # Delete later
            ],
        },
    },
    "extra_methods": ["get_entities"]}

# Mosaik API
class RadiatorSimulator(mosaik_api.Simulator):
    '''
    Simulator to be coupled with the cosimulation framework.

    :ivar str sid: String ID.
    :ivar dict entities: Dictionary containing simulation entities.
    :ivar dict _entities: Private dictionary containing simulation entities.
    :ivar dict eid_counters: Dictionary for entity ID counters.
    :ivar int time_resolution: Resolution in seconds.
    :ivar int step_size: Step size in seconds.
    :ivar int time_stamp: Current timestamp of the simulation.
    '''

    def __init__(self):
        '''
        Initializes the RadiatorSimulator.

        All methods in the inheritance chain are called.
        '''

        super(RadiatorSimulator, self).__init__(META)

        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, step_size=60):
        '''
        Initializes entities for simulation.

        :param str sid: String ID.
        :param int time_resolution: Resolution in seconds. Defaults to 1.
        :param int step_size: Step size in seconds. Defaults to 60.
        :return: Returns the meta data.
        :rtype: dict
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size
        self.time_stamp=0
        # return meta data
        return self.meta

    def create(self, num, model_type, model_params):
        '''
        Creates instances of the specified model_type. For example::
            design_params = {
                "t_supply_design": 60,
                "t_return_design": 35,
                "t_indoor_design": 22,
                "method": "LMTD",
                "Q_design": 5000,
                "n_coeff": 1.3}
                
        :param int num: Number of instances of the model_type.
        :param str model_type: Class specifying the model_type.
        :param dict model_params: Dictionary for the initialization.
        :return: List of entities created.
        :rtype: list
        

        '''
        # This is the Value to be returned in case PVSystem is not found.
        counter = self.eid_counters.setdefault(model_type, itertools.count())

        entities = []
        if model_params:
            self.model_params = model_params

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs

            self._entities[eid] = Radiator(params=model_params)

            full_id = '{}.{}'.format(self.sid, model_type)
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

            self.entities[eid] = {
                'ename': eid,
                'etype': model_type,
                'model': self._entities[eid],
                'full_id': full_id}

        return entities

    def step(self, time, inputs, max_advance=3600):
        '''
        Performs a simulation step.

        :param int time: Current simulation time.
        :param dict inputs: Inputs as given by the simulator.
        :param int max_advance: Max advance in seconds.
        :return: Next timestamp for simulation.
        :rtype: int
        '''

        # loop over input signals
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "t_supply":
                    self._entities[eid].t_supply = sum(values.values())
                if attr == "t_indoor":
                    self._entities[eid].t_indoor = sum(values.values())
                if attr == "Q_demand":
                    self._entities[eid].Q_demand = sum(values.values())
                if attr == "signal":
                    #print("val ---", sum(values.values()))
                    self._entities[eid].signal = sum(values.values())

        # Perform simulation step
            self._entities[eid].step()

        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        '''
        Retrieves data of the model after the step().

        :param dict outputs: Output parameters.
        :return: Data retrieved for the specified outputs.
        :rtype: dict
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self._entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["Radiator"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    if str(vars(entry)[attr]) not in ["nan"]:
                        # if attr == "massflow_in":
                        #     print(self.time_stamp, attr, vars(entry)[attr])
                        data[ename][attr] = vars(entry)[attr]
                    else:
                        # print("wrong atrr")
                        # print(self.time_stamp, attr, vars(entry)[attr])

                        data[ename][attr] = 0
        
        self.time_stamp+=self.time_resolution
        # return data to mosaik
        return data

    def get_entities(self):
        '''
        Retrieves entities of the API.

        :return: Entities of the API.
        :rtype: dict
        '''
        return self.entities
