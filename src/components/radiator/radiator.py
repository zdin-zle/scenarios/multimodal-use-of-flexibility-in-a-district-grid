'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------

A model for a radiator for house heating.
Based on the equations on the repository of https://github.com/DrTol/radiator_performance-Python

References
-------------------------------
Phetteplace - Optimal Design of Piping Systems for District Heating

Tol, Hİ. radiator_performance-Python. DOI: 10.5281/zenodo.3265381.

Tol, Hİ. District heating in areas with low energy houses -
Detailed analysis of district heating systems based on low temperature
operation and use of renewable energy. PhD Supervisors: Svendsen S. & Nielsen SB.
Technical University of Denmark 2015; 204 p. ISBN: 9788778773685.

'''
import math


class Radiator(object):
    '''
    Class representing a radiator unit.

    :ivar float t_supply: Radiator supply temperature [C].
    :ivar float t_return: Radiator return temperature [C].
    :ivar float t_indoor: Indoor temperature [C].
    :ivar float Q_demand: Heat Demand [W].
    :ivar float massflow_in: Radiator massflow [kg/s].
    :ivar float massflow_out: Radiator massflow [kg/s].
    :ivar int signal: Test signal (will be deleted later).
    :ivar dict radiator_status: Util dictionary to save the status to fetch data.
    :ivar float t_supply_design: Radiator design supply temperature [C].
    :ivar float t_return_design: Radiator design return temperature [C].
    :ivar float t_indoor_design: Indoor temperature design [C].
    :ivar str method: Calculation method (One of ["AMTD", "GMTD", "LMTD"]).
    :ivar float Q_design: Design heat demand [W].
    :ivar float n_coeff: Radiator experimental coefficient (1.1-1.5).
    :ivar list __design_params__: List of design parameters.
    :ivar str status: Status for logging purposes.

    '''
    # Variable Parameters
    t_supply = None       # Radiator supply temperature [C]
    t_return = None       # Radiator return temperature [C]
    t_indoor = None       # Indoor temperature [C]
    Q_demand = None     # Heat Demand [W]
    massflow_in = 0     # Radiator massflow [kg/s]
    massflow_out = 0     # Radiator massflow [kg/s]

    signal = 0  # Test--- delete later

    # Util dictionary to save the status to fetch data
    radiator_status = {}

    # Design Parameters
    t_supply_design = 60    # Radiator desing supply temperature [C]
    t_return_design = 35    # Radiator return temperature [C]
    t_indoor_design = 22    # Indoor temperature [C]
    # Calculatio method . One of ["AMTD", "GMTD", "LMTD"]
    method = "LMTD"
    Q_design = 5000         # Heat Demand [W]
    n_coeff = 1.3           # Radiator experimental coefficient 1.1-1.5

    __design_params__ = [
        "t_supply_design",
        "t_return_design",
        "t_indoor_design",
        "method",
        "Q_design",
        "n_coeff",
    ]

    # Status for logging
    status = None

    def __init__(self, params=None):
        '''
        Class constructor

        :param params: A dictionary containing the design parameters. Example::

            params = {
                "t_supply_design": 60,
                "t_return_design": 35,
                "t_indoor_design": 22,
                "method": "LMTD",
                "Q_design": 5000,
                "n_coeff": 1.3}

        '''
        if params:
            for key, value in params.items():
                if key in self.__design_params__:
                    setattr(self, key, value)

        self.signal = 0

    def step(self):
        '''
        Performs a simulation step.
        '''
        if self.method == "AMTD":
            self.t_return_AMTD()

        if self.method == "GMTD":
            self.t_return_GMTD()

        if self.method == "LMTD":
            self.t_return_LMTD()

        self.calc_massflow()

    def radiator_status(self):
        '''
        Returns the radiator status.

        :return: The radiator status dictionary.
        :rtype: dict
        '''
        self.radiator_status = self.__dict__

        return self.radiator_status

    def t_return_AMTD(self):
        '''
         Calculates the return temperature from the radiator unit - based on AMTD method.
        '''
        self.AMTD_design = self.AMTD(
            self.t_supply_design,
            self.t_return_design,
            self.t_indoor_design)

        self.t_return = 2 * (self.t_indoor + (self.AMTD_design *
                             (self.Q_demand / self.Q_design)**(1 / self.n_coeff))) - self.t_supply

        # Checking Approach factor
        AF = (self.t_return - self.t_indoor) / (self.t_supply - self.t_indoor)

        if AF >= 0.5:
            self.status = "Approach factor is %s - Error less than 0.04" % AF
        else:
            self.status = "Approach factor is %s - Error larger than 0.04" % AF

        if self.t_return >= self.t_supply or self.t_return <= 20:
            self.t_return = math.nan

    def t_return_GMTD(self):
        '''
        Calculates the return temperature from the radiator unit - based on
        GMTD, Geometric mean temperature distribution
        '''
        GMTD_design = self.GMTD(
            self.t_supply_design,
            self.t_return_design,
            self.t_indoor_design)

        self.t_return = self.t_indoor + ((self.t_supply - self.t_indoor)**(-1)
                                         * GMTD_design**2 * (self.Q_demand / self.Q_design)**(2 / self.n_coeff))

        # Checking Error
        AF = (self.t_return - self.t_indoor) / (self.t_supply - self.t_indoor)

        if AF >= 0.33:
            self.status = "Approach factor is %s - Error less than 0.05" % AF
        else:
            self.status = "Approach factor is %s - Error larger than 0.05" % AF

        if self.t_return >= self.t_supply:
            self.t_return = math.nan

    def t_return_LMTD(self):
        '''
        Calculates the return temperature from the radiator unit - based on
        LMTD, Logarithmic mean temperature distribution
        '''

        self.LMTD_design = self.LMTD(
            self.t_supply_design,
            self.t_return_design,
            self.t_indoor_design)

        # Iteration for the implicit LMTD method
        fTol = 0.001  # Iteration tolerance
        error = 10    # Iteration error

        # First set the return temperature based on GMTD method
        self.t_return_GMTD()
        t_return1 = self.t_return

        # Iteration loop
        while error > fTol:
            if self.Q_demand < self.Q_design/1000:
                t_return2 = self.t_indoor
            else:
                try:
                    t_return2 = self.t_indoor + ((self.t_supply - self.t_indoor) / math.exp(
                    (self.Q_demand / self.Q_design) ** (-1 / self.n_coeff) * (self.t_supply - t_return1) / self.LMTD_design))
                except:
                    t_return2=t_return1
            error = abs(t_return2 - t_return1)
            t_return1 = t_return2

        self.t_return = t_return2
        if self.t_return >= self.t_supply:
            self.t_return = math.nan

    def calc_massflow(self):
        '''
        Calculates resulting massflow based on the input/output temperatures.
        '''
        #self.t_supply = self.t_supply
        if self.t_return is not math.nan:
            self.massflow_in = self.Q_demand / 4180 / \
                (self.t_supply - self.t_return)

        else:
            #self.massflow_in = math.nan
            self.massflow_in = 0
            #print("massflow is 0")
        
        # if self.massflow_in is math.nan:
        #     self.massflow_in = 0

        self.massflow_out = -self.massflow_in

    def AMTD(self, t_supply, t_return, t_indoor):
        '''
        Returns Arithmet indoor Mean Temperature Difference (AMTD).

        :param float t_supply: Radiator supply temperature [C].
        :param float t_return: Radiator return temperature [C].
        :param float t_indoor: Indoor temperature [C].
        :return: Arithmet indoor Mean Temperature Difference.
        :rtype: float
        '''
        return (t_supply + t_return - 2 * t_indoor) / 2

    def GMTD(self, t_supply, t_return, t_indoor):
        '''
        Returns Geometric Mean Temperature Difference (GMTD).

        :param float t_supply: Radiator supply temperature [C].
        :param float t_return: Radiator return temperature [C].
        :param float t_indoor: Indoor temperature [C].
        :return: Geometric Mean Temperature Difference.
        :rtype: float
        '''
        return math.sqrt(t_supply - t_indoor) * math.sqrt(t_return - t_indoor)

    def LMTD(self, t_supply, t_return, t_indoor):
        '''
        Returns Logarithmic Mean Temperature Difference (LMTD).

        :param float t_supply: Radiator supply temperature [C].
        :param float t_return: Radiator return temperature [C].
        :param float t_indoor: Indoor temperature [C].
        :return: Logarithmic Mean Temperature Difference.
        :rtype: float
        '''
        return (t_supply - t_return) / \
            math.log((t_supply - t_indoor) / (t_return - t_indoor))


if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd

    temps_in = np.arange(50, 72, 2).tolist()
    t_indoors = np.full(shape=len(temps_in), fill_value=20).tolist()
    Q_demand = np.full(shape=len(temps_in), fill_value=4000).tolist()

    design_params = {
        "t_supply_design": 60,
        "t_return_design": 35,
        "t_indoor_design": 22,
        "method": "LMTD",
        "Q_design": 5000,
        "n_coeff": 1.3}

    radiator = Radiator(design_params)
    t_out = []
    massflows = []
    for i in range(len(temps_in)):
        radiator.t_supply = temps_in[i]
        radiator.Q_demand = Q_demand[i]
        radiator.t_indoor = t_indoors[i]
        radiator.step()
        t_out.append(round(radiator.t_return, 2))
        massflows.append(round(radiator.massflow_in, 4) * 1000)

    data = {
        "Input -C": temps_in,
        "Outputs -C": t_out,
        "Massflow -g/s": massflows}

    df = pd.DataFrame.from_dict(data)
    df.plot()
    plt.show()
