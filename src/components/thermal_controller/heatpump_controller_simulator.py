'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**

Jan Philipp Hörding **@UOL/OFFIS**

Description
-------------------------------

HeatpumpControllerSimulator is a simulator designed to control a heat pump object within a system where temperature signals 
from tanks drive the heat pump's output demand.

Functionality
-------------------------------
The primary functionalities of this simulator include:

- `init`: Initializes the simulation by setting properties such as time resolution, start date, and step size. It allows for defining parameters related to temperature thresholds, minimum demand for heat pump operation, and maximal input temperature to avoid malfunctioning.

- `create`: Creates an instance of the specified model type. It ensures only one entity is created and allows for setting model-specific parameters if provided.

- `step`: Drives the simulation by implementing control logic. This method processes inputs, including temperature and demand signals, and calculates the controlled heat demand based on defined rules and environmental factors.

- `get_data`: Retrieves output data based on requested attributes, delivering simulation results related to temperature signals, controlled demand, and other relevant parameters.

The core functionality of this simulator revolves around regulating the heat pump's operation based on temperature inputs from tanks and demand signals. It incorporates control logic to ensure the heat pump operates within safe temperature limits while meeting the demand for heating or cooling as per the specified conditions.


Architecture
-------------------------------
Following is the controller architecture

::

> Tank Measurements ----> | Controller Logic | ----> Heatpump set demand
>                         |                  | 
> Heat Demand  Signal --> |                  |


'''

import itertools
import mosaik_api

# Simulation Metadata
META = {
    'type': 'time-based',
    'models': {
        'HeatpumpController': {
            'public': True,
            'params': ["model_params", "controlled_entities"],
            'attrs': [
                "Q_demand_signal",         # Signal of Q_demand
                "T_in_tank",               # Temperature input from the tank
                "T_mean_tank",             # Mean Temperature of the tank
                "Q_Demand_controlled"      # Controlled value of demand
            ],
        },
    },
}


class HeatpumpControllerSimulator(mosaik_api.Simulator):
    '''
    Simulator to control a heat pump object
    '''

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called
        '''
        super(HeatpumpControllerSimulator, self).__init__(META)
        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}

    def init(self, sid, time_resolution=1, start_date=None, step_size=60):
        '''
        Initialization of entities.

        :param sid: String ID.
        :param int time_resolution: Resolution in seconds.
        :param start_date: Start date. Format: 'YYYY-MM-DD hh:mm:ss'.
        :param int step_size: Step size in seconds.
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        self.min_temperature = None     # Minimal temperature of heatpump entry
        self.min_tank_mean_T = None     # Desired minimal mean tank temperature
        self.Q_demand_min = None        # Minimum demand for the heatpump to operate
        # Maximal input temperature to avoid heatpump malfunctioning
        self.max_input_temperature = None

        # return meta data
        return self.meta

    def create(self, num, model_type, model_params=None):
        '''
        Creates instances of the specified model_type.

        :param int num: Number of instances of the model_type.
        :param class model_type: Class specifying the model_type.
        '''
        if num != 1:
            raise ValueError("Only one entity can be created")

        if model_params:
            for attr, val in model_params.items():
                setattr(self, attr, val)

        counter = self.eid_counters.setdefault(model_type, itertools.count())

        entities = []

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs
            self._entities[eid] = None
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

        return entities

    def step(self, time, inputs, max_advance=36000):
        '''
        Perform simulation step.

        :param float time: Current simulation time.
        :param dict inputs: Inputs as given by the simulator.
        :param int max_advance: Maximum advance in seconds.
        '''
        # Perform simulation step
        self.cache = {}

        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "Q_demand_signal":
                    q_demand_signal = sum(values.values())
                elif attr == "T_in_tank":
                    t_tank_input = sum(values.values())
                elif attr == "T_mean_tank":
                    t_mean_tank = sum(values.values())

        #===================================================================
        # Controller logic
        #===================================================================
        Q_Demand_controlled = 0

        # Turn off if water is too cold at the input
        if t_tank_input < self.min_temperature:
            Q_Demand_controlled = 0

        # Turn off if water is too hot at the output
        elif t_tank_input > self.max_input_temperature:
            Q_Demand_controlled = 0
        else:
            # If the tank is cold, force a heating step
            if t_mean_tank < self.min_tank_mean_T:

                # Choose the bigger of the values between the demand and the
                # minimum
                if self.Q_demand_min < q_demand_signal:
                    Q_Demand_controlled = q_demand_signal

                else:
                    Q_Demand_controlled = self.Q_demand_min

            # If the tank is hot enough, heat only the amount extracted
            else:
                if self.Q_demand_min < q_demand_signal:
                    Q_Demand_controlled = q_demand_signal

                else:
                    Q_Demand_controlled = 0

        #======================================================================
        # End of Logic
        #======================================================================

        self.cache = {}
        for eid in self._entities.keys():
            self.cache[eid]={}
            self.cache[eid]['Q_demand_signal']=q_demand_signal
            self.cache[eid]['T_in_tank'] = t_tank_input
            self.cache[eid]['T_mean_tank'] = t_mean_tank
            self.cache[eid]['Q_Demand_controlled']= Q_Demand_controlled

        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        '''
        Retrieve output data.

        :param dict outputs: Output values to retrieve data for.
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["HeatpumpController"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    if attr in ['Q_demand_signal', 'T_in_tank', 'T_mean_tank', 'Q_Demand_controlled']: #== "Q_Demand_controlled":
                        data[ename][attr] = float(self.cache[ename][attr])

        return data


if __name__ == "__main__":
    pass
