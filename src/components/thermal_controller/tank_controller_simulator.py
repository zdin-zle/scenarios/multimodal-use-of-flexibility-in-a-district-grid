'''
Authors
-------------------------------
Fernando Penaherrera **@UOL/OFFIS**


Description
-------------------------------

TankControllerSimulator is a simulator designed to simulate the behavior of a tank's temperature control system.

This simulator is specifically tailored to manage and regulate the temperature within a tank by adjusting various parameters 
based on sensor inputs and control signals. 

Functionality
-------------------------------
It operates through several key methods:

- `init`: Initializes the simulation by setting parameters such as time resolution and step size. It allows for configuration adjustments related to minimum temperature settings for heat pump entry and upper temperature limits.

- `create`: Creates instances of the TankController model type, enabling control over tank entities. It ensures that only one tank can be controlled and retrieves necessary data regarding heating rods, maximum temperature, and thermal power.

- `step`: Drives the simulation by executing control logic. This method receives inputs, such as sensor readings and control signals, and determines the appropriate actions based on defined logic. It computes the power output for heating the tank and updates various internal parameters accordingly.

- `get_data`: Provides output data based on requested attributes. It retrieves and formats simulation data related to the tank controller's current state, including temperature readings, control signals, state of charge (SOC), and other relevant parameters.

The main functionality revolves around controlling the tank's temperature by adjusting the heating power based on sensor inputs and control signals. It incorporates complex logic to manage the heating system, ensuring the tank's temperature stays within desired limits while considering various environmental factors and system constraints.

'''
import itertools
import mosaik_api
import mosaik

# Simulation Metadata

META = {
    'type': 'time-based',
    'models': {
        'TankController': {
            'public': True,
            'params': ["model_params", "controlled_entities"],
            'attrs': [
                "signal",             # Double value of the input signal for the heat rod [W]
                "T_air",               # outside air temperature
                "T_mean_tank",        # Mean temperature of the tank [C]
                "T_upper_sensor",     # Temperature of the upper sensor [C]
                "T_heatpump_sensor",  # Temperature of the sensor at the heat pump [C]
                "P_th_set",            # Output signal for output of the heat rod [W]
                "P_th_max",            # Max Power of the heating rod. Related to the max. allowed temperature [W]
                "SOC",                 # Output SOC of the tank relative to the outside air temperature [1] 
                "T_max",
                "case",
                "mass"  
            ],
        },
    },
}


class TankControllerSimulator(mosaik_api.Simulator):
    """ Tank Controller Simulator 

    :param mosaik_api: Inherites a mosaik_api.Simulator class
    :type mosaik_api: mosaik_api.Simulator class
    """

    def __init__(self):
        '''
        Initialization
        All methods in the inheritance chain are called
        '''
        super(TankControllerSimulator, self).__init__(META)
        self.sid = None
        self.entities = {}
        self._entities = {}
        self.eid_counters = {}
        self.cache={}
        self.P_th_max = None
        self.mass = None

    def init(self, sid, time_resolution=1, step_size=60, config= None):
        '''
        Initialization of entities.

        :param str sid: String ID.
        :param int time_resolution: Resolution in seconds.
        :param int step_size: Step size in seconds.
        :param dict config: Configuration parameters for the simulation.
        '''

        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size
        # return meta data
        self.min_temp_heatpump_entry = 10
        self.min_T_upper = 55
        if config:
            self.min_temp_heatpump_entry = config["min_temp_heatpump_entry"]
            self.min_T_upper = config["min_T_upper"]

        return self.meta

    def create(self, num, model_type, controlled_entities=None):
        '''
        Creates instances of the specified model_type.

        :param int num: Number of instances of the model_type.
        :param class model_type: Class specifying the model_type.
        :param str controlled_entity: Tank Entity to be controlled.
        '''

        
        counter = self.eid_counters.setdefault(model_type, itertools.count())

        #tank_entities_dict =controlled_entities.sim.proxy.sim.models
        if mosaik._version.VERSION in ["3.0.0","3.1.0"]:
            tank_entities_dict = controlled_entities.sim._inst.models
        else:
            tank_entities_dict =controlled_entities.sim.proxy.sim.models
        if len(tank_entities_dict.keys()) > 1:
            raise ValueError("Only one tank can be controlled")

        for _, models in tank_entities_dict.items():
            self.controlled_entities = models

        for k in self.controlled_entities.heating_rods.keys():
            # There is only one
            # "self.controlled_entities.heating_rods[k]" is the heating rod
            self.max_temperature = self.controlled_entities.heating_rods[k].T_max
            self.P_th_max_design = self.controlled_entities.heating_rods[k].P_th_max

        entities = []

        for _ in range(num):
            eid = '{}_{}'.format(model_type, next(counter))  # Entities IDs
            entities.append({'eid': eid, 'type': model_type, 'rel': []})

        return entities

    def step(self, time, inputs, max_advance=36000):
        '''
        Perform simulation step.

        :param float time: Current simulation time.
        :param dict inputs: Inputs as given by the simulator.
        :param int max_advance: Maximum advance in seconds.
        '''
        self.cache= {}
        # Perform simulation step
        self.cache = {}
        control_signal = 0
        for _, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                if attr == "signal":
                    control_signal = sum(values.values())
                if attr == "T_upper_sensor":
                    T_upper_sensor = sum(values.values())
                if attr == "T_heatpump_sensor":
                    T_heatpump_sensor = sum(values.values())
                if attr == "T_air":
                    T_air  = sum(values.values())    
                if attr == "T_mean_tank":
                    T_mean_tank  = sum(values.values())    
                if attr == "mass":
                    self.mass  = sum(values.values())                                    

        #======================================================================
        # Here goes the controller logic
        #======================================================================
        soc = (T_mean_tank-T_air)/(self.max_temperature-T_air)
        
        # Information about the use case of the heating
        c= 0
        max_P_upper = self.mass/1000*4180*(self.max_temperature-T_upper_sensor)/5 # Temporal solution due to number of layers
        self.P_th_max = self.P_th_max_design
        # Turn off if the max temperature is 90 C
        if T_upper_sensor > self.max_temperature:
            P_th_set = 0
            self.P_th_max = 0

        # Force heating if tank is too cold
        else:
            if max_P_upper< self.P_th_max_design:
                self.P_th_max = max_P_upper
                P_th_set = max_P_upper

            # Heat if the heat pump entry is too cold
            if T_heatpump_sensor < self.min_temp_heatpump_entry:
                P_th_set = self.P_th_max
                c= 1
            else:
                # Heat if the upper layer is too cold
                if T_upper_sensor<self.min_T_upper:
                    P_th_set = self.P_th_max
                else: 
                    # In case of an error and an negative signal
                    if control_signal <= 0:
                        P_th_set = 0
                        c=2
                    else:
                        # Set the value with the control signal
                        if control_signal > self.P_th_max:
                            P_th_set = self.P_th_max
                            c=3
                        else:
                            P_th_set = control_signal
                            c=4

        #=======================================================================
        # End of controler logic        
        #=======================================================================

        cache = {}
        cache["signal"] = control_signal # Double value of the input signal for the heat rod [W]
        cache["T_air"] = T_air               # outside air temperature
        cache["T_mean_tank"] = T_mean_tank  # Mean temperature of the tank [C]
        cache["T_upper_sensor"] = T_upper_sensor # Temperature of the upper sensor [C]
        cache["T_heatpump_sensor"] = T_heatpump_sensor        # Temperature of the sensor at the heat pump [C]
        cache["P_th_set"] = P_th_set        # Output signal for output of the heat rod [W]
        cache["SOC"] = soc        # Output SOC of the tank relative to the outside air temperature [1]
        cache["T_max"] = self.max_temperature
        cache["case"] = c
        cache["P_th_max"] = self.P_th_max
        cache["mass"] = self.mass
        self.cache = cache

       
        return time + self.step_size

    def get_data(self, outputs):
        '''
        Retrieve output data.

        :param dict outputs: Output values to retrieve data for.
        '''
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models']["TankController"]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                else:
                    data[ename][attr] = float(self.cache[attr])
        
        return data


if __name__ == "__main__":
    pass
