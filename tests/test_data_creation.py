
import sys
import os
path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)

import unittest
from src.scenarios.util import read_system_data


class TestReadSystemDataFunction(unittest.TestCase):
    
    def test_read_system_data(self):
        # Assuming create_heat_system_data function is working correctly
        
        # Call the function
        data_jsons = read_system_data()
        
        # Assert that the returned data is a dictionary
        self.assertIsInstance(data_jsons, dict)
        
        # Add more specific assertions based on the expected structure of the loaded data
        for key in ['building_data', 'heat_pump_data', 
                    'radiators_data', 'tank_data', 
                    'tank_init_vals', 'heat_pump_ctrl_data']:
            self.assertIn(key, data_jsons)
    
        
        for key in data_jsons['tank_data'].keys():
            self.assertIsInstance(data_jsons['tank_data'][key]['n_layers'], int)
            self.assertIsInstance(data_jsons['tank_data'][key]['n_sensors'], int)



if __name__ == '__main__':
    unittest.main()