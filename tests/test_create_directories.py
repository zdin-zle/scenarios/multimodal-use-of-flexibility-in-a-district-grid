#if __name__ == "__main__":
import sys
import os
path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)

import unittest
import os

from src.common import *



class TestCreateDirectories(unittest.TestCase):
    def test_directories_creation(self):
        # Define the directories that should be created
        directories = [
        BASE_DIR_SRC, DATA_DIR, DATA_PROCESSED_DIR, DATA_RAW_DIR,
        RESULTS_DIR, RESULTS_DB_DIR, RESULTS_FIGS_DIR,
        RESULTS_KIPS_DIR, RESULTS_OP_PLAN_DIR, SCENARIOS_DIR, PREPROC_DIR,
        POSTPROC_DIR, TEST_DIR
    ]

        # Run the function that creates directories
        create_directories(directories)

        # Check if the directories were created
        for directory in directories:
            self.assertTrue(os.path.exists(directory), f"Directory '{directory}' was not created.")

if __name__ == '__main__':
    unittest.main()