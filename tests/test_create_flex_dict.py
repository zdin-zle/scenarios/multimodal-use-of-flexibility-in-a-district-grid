import sys
import os
path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)

import unittest
from src.postprocessing.create_flex_pickles import create_flex_dict, save_dicts_to_pickle
from src.common import Scenarios


class TestCreateFlexDictFunction(unittest.TestCase):

    def setUp(self):
        # Set up any necessary variables or configurations
        pass

    def tearDown(self):
        # Clean up any resources if needed
        pass

    def test_create_flex_dict_with_valid_scenario(self):
        # Example configuration with a valid scenario
        example_configuration = {
            "scenario": Scenarios.OP_PLAN,
            "n_buildings": 6  # Replace 2 with the actual number of buildings
        }

        database_filename = os.path.join(
            os.getcwd(),"tests", "test_data", "Results_OP_PLAN.hdf5")

        # Call the function
        flex_list = create_flex_dict(example_configuration, database_filename)

        # Assert that the returned result is a list
        self.assertIsInstance(flex_list, list)

        self.assertEqual(len(flex_list), example_configuration["n_buildings"])
        for idx in range(len(flex_list)):
            self.assertIsInstance(flex_list[idx], dict)

            self.assertIn("flex_max_power", flex_list[idx])
            self.assertIn("flex_min_power", flex_list[idx])
            self.assertIn("schedule", flex_list[idx])
            self.assertIn("max_energy_delta", flex_list[idx])
            self.assertIn("flex_max_energy_delta", flex_list[idx])
            self.assertIn("flex_min_energy_delta", flex_list[idx])

        for key in flex_list[idx].keys():
            self.assertEqual(24, len(flex_list[idx][key]))

    def test_create_flex_dict_with_invalid_scenario(self):
        # Example configuration with an invalid scenario
        example_configuration = {
            "scenario": Scenarios.FLEX,
            "n_buildings": 6
        }
        database_filename = os.path.join(
            os.getcwd(), "test_data", "Results_OP_PLAN.hdf5")

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError):
            create_flex_dict(example_configuration, database_filename=database_filename)


class TestSaveDictsToPickleFunction(unittest.TestCase):

    def setUp(self):
        # Set up any necessary variables or configurations
        pass

    def tearDown(self):
        # Clean up any resources if needed
        pass

    def test_save_dicts_to_pickle(self):
        # Example flexibility data
        example_flex_data = [
            {"flex_max_power": [100, 200],
             "flex_min_power": [50, 100],
             "schedule": [0,1000],
             "max_energy_delta": [5000, 10000],
             "flex_max_energy_delta": [500, 1000],
             "flex_min_energy_delta": [0, 0]},
            {"flex_max_power": [150, 250],
             "flex_min_power": [75, 125],
             "schedule": [500,1000],             
             "max_energy_delta": [7500, 12500],             
             "flex_max_energy_delta": [750, 1250],
             "flex_min_energy_delta": [0, 0]}
        ]

        # Example configuration
        example_configuration = {
            "scenario": Scenarios.OP_PLAN,  # Replace with the actual scenario
        }

        # Call the function
        folder_name = os.path.join(os.getcwd(), "tests", "temp")
        saved_files = save_dicts_to_pickle(example_flex_data,
                                           example_configuration,
                                           folder_name=folder_name)

        # Assert that the returned result is a dictionary
        self.assertIsInstance(saved_files, dict)

        self.assertEqual(len(saved_files), len(example_flex_data))
        self.assertTrue(all(os.path.exists(file_path)
                        for file_path in saved_files.values()))

        # Clean up: Remove the created pickle files
        for file_path in saved_files.values():
            if os.path.exists(file_path):
                os.remove(file_path)


if __name__ == '__main__':
    unittest.main()
