import sys
import os
path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)
    
import unittest
from src.scenarios.scenario import create_world
from src.common import Scenarios


class TestCreateWorldFunction(unittest.TestCase):
    
    def test_op_plan_scenario(self):
        configuration = {"scenario": Scenarios.OP_PLAN}
        world = create_world(configuration)
        self.assertIsNotNone(world)
        # Add more assertions or checks if necessary

    def test_op_plan_scenario(self):
        configuration = {"scenario": Scenarios.FLEX}
        world = create_world(configuration)
        self.assertIsNotNone(world)
        # Add more assertions or checks if necessary

        
    def test_other_scenario(self):
        configuration = {"scenario": Scenarios.TEST}  # Replace SOME_OTHER_SCENARIO with the actual scenario value
        world = create_world(configuration)
        self.assertIsNone(world)
        # Add more assertions or checks if necessary

if __name__ == '__main__':
    unittest.main()