import sys
import os

path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)

import unittest
from src.components.radiator.radiator import Radiator


class TestRadiator(unittest.TestCase):
    def setUp(self):
        # Initialize the Radiator object with default parameters
        self.radiator = Radiator()

    def test_default_values(self):
        # Test if default values are set correctly
        self.assertIsNone(self.radiator.t_supply)
        self.assertIsNone(self.radiator.t_return)
        self.assertIsNone(self.radiator.t_indoor)
        self.assertIsNone(self.radiator.Q_demand)
        self.assertEqual(self.radiator.massflow_in, 0)
        self.assertEqual(self.radiator.massflow_out, 0)
        self.assertEqual(self.radiator.signal, 0)
        self.assertEqual(self.radiator.t_supply_design, 60)
        self.assertEqual(self.radiator.t_return_design, 35)
        self.assertEqual(self.radiator.t_indoor_design, 22)
        self.assertEqual(self.radiator.method, "LMTD")
        self.assertEqual(self.radiator.Q_design, 5000)
        self.assertAlmostEqual(self.radiator.n_coeff, 1.3)
        self.assertListEqual(
            self.radiator.__design_params__,
            [
                "t_supply_design",
                "t_return_design",
                "t_indoor_design",
                "method",
                "Q_design",
                "n_coeff",
            ],
        )
        self.assertIsNone(self.radiator.status)

    def test_AMTD_calculation(self):
        # Test AMTD calculation method

        design_params = {
        "t_supply_design": 60,
        "t_return_design": 35,
        "t_indoor_design": 22,
        "method": "LMTD",
        "Q_design": 5000,
        "n_coeff": 1.3}

        self.radiator = Radiator(design_params)
        self.radiator.method = "AMTD"
        self.radiator.t_supply_design = 60
        self.radiator.t_return_design = 35
        self.radiator.t_indoor_design = 22
        self.radiator.Q_demand = 4000  # Change Q_demand for testing
        self.radiator.t_indoor = 16
        self.radiator.t_supply = 45
        self.radiator.step()
        # Assert the calculated return temperature and status
        print(self.radiator.t_return)
        self.assertAlmostEqual(self.radiator.t_return, 29.956, places = 2)
        self.assertIn("Approach factor", self.radiator.status)

    def test_GMTD_calculation(self):
        # Test GMTD calculation method
        self.radiator.method = "GMTD"
        self.radiator.t_supply_design = 60
        self.radiator.t_return_design = 35
        self.radiator.t_indoor_design = 22
        self.radiator.Q_demand = 4000  # Change Q_demand for testing
        self.radiator.t_indoor = 16
        self.radiator.t_supply = 45
        self.radiator.step()
        # Assert the calculated return temperature and status
        self.assertAlmostEqual(self.radiator.t_return, 28.085, places = 3)
        self.assertIn("Approach factor", self.radiator.status)

    def test_LMTD_calculation(self):
        # Test LMTD calculation method
        self.radiator.method = "LMTD"
        self.radiator.t_supply_design = 60
        self.radiator.t_return_design = 35
        self.radiator.t_indoor_design = 22
        self.radiator.Q_demand = 4000  # Change Q_demand for testing
        self.radiator.t_indoor = 16
        self.radiator.t_supply = 45
        self.radiator.step()
        # Assert the calculated return temperature and status
        self.assertAlmostEqual(self.radiator.t_return, 28.535, places = 3)
        self.assertIn("Approach factor", self.radiator.status)

        # You can also test additional scenarios, for instance, when Q_demand < Q_design/1000
        self.radiator.Q_demand = 0  # Change Q_demand for testing (less than Q_design/1000)
        self.radiator.step()
        self.assertAlmostEqual(self.radiator.t_return, 16)
        self.assertIn("Approach factor", self.radiator.status)


if __name__ == "__main__":
    unittest.main()
