import sys
import os
path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)

import unittest
from src.scenarios.util import scale_system_data

class TestScaleSystemDataFunction(unittest.TestCase):
    
    def test_scale_system_data_with_scaling_1(self):
        # Example system data with scaling factor 1
        system_data = {
            'building_data': {1: {'max_occupancy': 100}},
            'heat_pump_data': {1: {'hp_model': 'Air_30kW'}},
            'radiators_data': {1: {'Q_design': 500}},
            'tank_data': {1: {'diameter': 100, 'heating_rods': {'hr_1': {'P_th_stages': [0, 100, 200]}}}}
        }
        
        # Call the function with scaling factor 1
        scaled_data = scale_system_data(system_data, scaling=1)
        
        # Assert that the scaled data is the same as the input data
        self.assertEqual(scaled_data, system_data)
    
    def test_scale_system_data_with_scaling_greater_than_1(self):
        # Example system data with scaling factor greater than 1
        system_data = {
            'building_data': {1: {'max_occupancy': 100}},
            'heat_pump_data': {1: {'hp_model': 'Air_30kW'}},
            'radiators_data': {1: {'Q_design': 500}},
            'tank_data': {1: {'diameter': 100, 'heating_rods': {'hr_1': {'P_th_stages': [0, 100, 200]}}}}
        }
        
        # Call the function with scaling factor greater than 1 (e.g., 2)
        scaled_data = scale_system_data(system_data, scaling=2)
        
        # Assert that the scaled data has been modified based on the scaling factor
        # Add more specific assertions based on the modifications made by the function
        self.assertEqual(scaled_data['building_data'][1]['max_occupancy'], 200)
        self.assertEqual(scaled_data['heat_pump_data'][1]['hp_model'], 'Air_60kW')
        self.assertEqual(scaled_data['radiators_data'][1]['Q_design'], 1000)
        self.assertEqual(scaled_data['tank_data'][1]['diameter'], 141.4213562373095)
        self.assertEqual(scaled_data['tank_data'][1]['heating_rods']['hr_1']['P_th_stages'], [0, 200, 400])

if __name__ == '__main__':
    unittest.main()

