""" Main simulation file

Imports the simulation scenario and runs a simulation based on the given configuration

"""

from src.common import Scenarios
from src.scenarios.scenario import main as main_scenario

if __name__ == "__main__":
    configuration = {
        "scenario": Scenarios.FLEX,
        "scaling": 5,
        "start": '2020-01-01 00:00:00',
        "end": 1*24*3600,
        "step_size": 3600,
        "n_buildings": 6
    }

    main_scenario(configuration=configuration)