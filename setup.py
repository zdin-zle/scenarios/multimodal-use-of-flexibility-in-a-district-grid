from setuptools import setup, find_packages

setup(
   name='ZLE-Flex',
   version='0.2.0',
   author='Fernando Penaherrera, ',
   author_email='s',

   url='https://gitlab.com/zdin-zle/scenarios/multimodal-use-of-flexibility-in-a-district-grid',
   include_package_data=True,



   # url='http://pypi.python.org/pypi/PackageName/',
   license='LICENSE.txt',
   description='Small example of a neighborhood grid using flexibility of heat pumps to solve grid problems',
   long_description=open('README.txt').read(),
   install_requires=[

#TODO!

   ],
)