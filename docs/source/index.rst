.. Multimodal Use of Flexibility in a District Grid documentation master file, created by
   sphinx-quickstart on Fri Dec 15 11:32:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Multimodal Use of Flexibility in a District Grid's documentation!
============================================================================

**Multimodal Use of Flexibility in a District Grid** is a simulation study that 
evaluates the implementation of flexibility through optimization for the 
evaluation of operational plans for thermal energy supply in a district level.

The repository aims to analyze the effects of multimodal flexibility in the neighborhood district to solve problems in the power grid.

It implements several components for a co-simulation, using a flexibility agent to
calculate optimal schedules for operation, and thus avoid load peaks. 

The :doc:`overview` section provides a deeper description of the structure of the 
case study, and of the models used for the cosimulation.

Check out the :doc:`usage` section for further information, including how to 
:ref:`install <repository_installation>` the repository to run the simulations locally.

Additionally, results of the simulation are previewed in the 
:doc:`results` section

.. note:: 

   This project is under active development from the `ZLE Research group <https://www.zdin.de/zukunftslabore/energie>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   autoapi/index
   usage
   overview
   results
   authors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

