Structure
========================================

Introduction
----------------------------------------

Within this scenario, flexibility of a heating, ventilation, and air conditioning (HVAC) system is used to solve 
problems in the electric grid. Building thermal energy demand, including their HVAC are simulated. The flexibility of their HVAC 
is determined and translated in a uniform flexibility description. The flexibility of multiple buildings is aggregated 
and used when the grid simulation announces grid problems (e.g. voltage or current violations). The grid simulation 
includes load profiles of PV systems and households.

The first proposal of the model was originally thought to include grid control in the simulation:

.. image:: figures/Multimodal_Use_of_Flexibility_in_a_District_Grid.png
  :width: 1000
  :alt: Power consumption in the tanks


The thermal components are based on the `Mosaik-Heatpump <https://gitlab.com/mosaik/components/energy/mosaik-heatpump.git>`_ components.
`COHDA <https://gitlab.com/zdin-zle/models/mosaik-cohda.git>`_ is used for the aggregation of flexibility.
`mango <https://gitlab.com/mango-agents/mango>`_ is used as multi agent environment for COHDA. All simulators
will be coupled using the co-simulation tool `mosaik <https://gitlab.com/mosaik>`_.  


Simulation Architecture
----------------------------------------
This code performs a computer simulation of a multi-modal building system with heating components.
The main simulation file `src.scenarios.scenario` orchestrates a comprehensive simulation of
a multi-modal building system with various heating components and controllers, allowing for the analysis
and comparison of scenarios (e.g., operational planning versus flexibility) by examining their 
impact on the system's components and energy use.

Following is the structure of the simulation:

Inputs:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- **Configuration Data**: Configuration parameters dictate the scenario, scaling, simulation duration, step size, and the number of buildings involved.
- **System Data**: Data regarding buildings, heating devices (heat pumps, radiators), hot water tanks, meteorological data, etc.

Architecture of the Computer Simulation:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. **Creation of Mosaik World Object**: Based on the provided configuration, it sets up a Mosaik world object that acts as the simulation environment.
2. **Simulation Configuration**: It configures the simulation scenario based on the world object and provides configuration data. This step involves reading system data, scaling it, and preparing the simulation for scenarios like Operational Planning (OP_PLAN) or Flexibility (FLEX).
3. **Starting Simulators**: Initializes various simulators (e.g., for databases, meteorological data, building components like heat pumps, tanks, radiators, controllers, etc.).
4. **Creating Models**: Creates various models based on the initial data and design parameters.
5. **Connecting Simulators**: Establishes connections between different models to simulate interactions between components (e.g., connecting meteo data to building models, connecting controllers to tanks and heating devices).
6. **Run Simulation**: Executes the simulation loop until the specified end time, gathering data and storing results into a database.

Outputs:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- **Simulation Results**: Outputs from the simulation are stored in databases, such as temperature readings, heating demands, controlled values, and other relevant parameters for each building and its components.
- **Post-Processing and Comparison**: After running the simulation, it generates visual comparisons (e.g., between different scenarios or models) using the obtained database results. This includes comparison plots for hot water tanks and radiators.

Components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. list-table:: Overview of component list
   :widths: 25 25 50
   :header-rows: 1

   * - Simulation Object
     - Simulator Name
     - Source Code
   * - Hot Water Tank	
     - Mosaik-HotWaterTankSim
     - https://gitlab.com/mosaik/components/energy/mosaik-heatpump.git
   * - Heat Pump	
     - Mosaik-Heatpump	
     - https://gitlab.com/mosaik/components/energy/mosaik-heatpump.git
   * - Radiator	
     - Mosaik-Radiator	
     - https://gitlab.com/zdin-zle/scenarios/multimodal-use-of-flexibility-in-a-district-grid.git
   * - Heat Pump Controller	
     - Mosaik-HPController	
     - https://gitlab.com/zdin-zle/scenarios/multimodal-use-of-flexibility-in-a-district-grid.git
   * - Tank Controller	
     - Mosaik-TankController	
     - https://gitlab.com/zdin-zle/scenarios/multimodal-use-of-flexibility-in-a-district-grid.git
   * - CSV Data Reader	
     - Mosaik-CSV	
     - https://gitlab.com/mosaik/components/data/mosaik-csv.git
   * - HDF5 Database	
     - Mosaik-HDF5	
     - https://gitlab.com/mosaik/components/data/mosaik-hdf5.git
   * - Building	
     - Mosaik-RCBuilding	
     - https://gitlab.com/zdin-zle/models/rc-building.git
   * - Multi Agent System	
     - Mosaik-Cohda	
     - https://gitlab.com/zdin-zle/models/mosaik-cohda.git
   * - Pickle Reader	
     - Mosaik-Cohda-Pickle	
     - https://gitlab.com/zdin-zle/models/mosaik-cohda.git
   * - Operational Plan Reader	
     - Mosaik-Cohda-OPPlan	
     - https://gitlab.com/zdin-zle/models/mosaik-cohda.git




