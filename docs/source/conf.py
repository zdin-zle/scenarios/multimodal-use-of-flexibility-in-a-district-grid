# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Multimodal Use of Flexibility in a District Grid'
copyright = '2023, Fernando Penaherrera, Jan Philipp Hörding'
author = 'Fernando Penaherrera, Jan Philipp Hörding'
release = '0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'autoapi.extension'
]
autoapi_type = 'python'
autoapi_dirs = ['../../src']
templates_path = ['_templates']
autoapi_template_dir = "_templates/autoapi"
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ['_static']
