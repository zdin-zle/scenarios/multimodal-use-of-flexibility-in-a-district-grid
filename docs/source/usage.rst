Usage
========================================

.. _repository_installation:

Repository Installation
----------------------------------------

To use the models provided in this repository, follow these steps:

1. Clone the repository:

   .. code-block:: bash

      git clone https://gitlab.com/zdin-zle/scenarios/multimodal-use-of-flexibility-in-a-district-grid.git

2. Install the necessary dependencies. Refer to the `requirements.txt` file for a list of required packages and their versions:

   .. code-block:: bash

      pip install -r requirements.txt


3. To run the simulation, the main file can be directly run:

   .. code-block:: bash

      python main.py

4. If running from an IDE, the following configuration parameters can be altered to evaluate different scenarios

.. code-block:: python

   from src.common import Scenarios
   from src.scenarios.scenario import main as main_scenario

   if __name__ == "__main__":
      configuration = {
         "scenario": Scenarios.FLEX,
         "scaling": 5,
         "start": '2020-01-01 00:00:00',
         "end": 1*24*3600,
         "step_size": 3600,
         "n_buildings": 6
      }

      main_scenario(configuration=configuration)


5. The results are saved in a database with a timestamp, in the databases folder:

.. code-block:: bash

   REPO_PATH/src/results/databases

6. Figures are saved in a figure folder with a timestamp, under

.. code-block:: bash

  REPO_PATH/src/results/figures
