Examples
========================================

.. _results:

Example of Results
----------------------------------------
The main results show the effects of incorporating flexibility on the grid. This show the
changes on the original operational plan to avoid peaks on the grid. Six houses are simulated with their heat pumps,
each connected to a storage tank, which is shown in the plot.

.. image:: figures/power.png
  :width: 800
  :alt: Power consumption in the tanks

These different power inputs in the tank result in different temperatures for the tank at various levels.

.. image:: figures/t_mean.png
  :width: 800
  :alt: Building Models

The results also show the outputs of the heat demand of the buildings. Since the buildings are quite
similar, the demand profiles are similar as well.

.. image:: figures/q_demand.png
  :width: 800
  :alt: Thermal Demand of Buildings





